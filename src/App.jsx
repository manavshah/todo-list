import React, { useState } from 'react';
import ToDoLists from './ToDoLists';


const App = () => {
    const [inputList, setInputList] = useState("");
    const [Items, setItems] = useState([]); // yaha mujhe empty array bana na tha ki koi user typr kre to empty array me store hojaye & phir ye array currentelemnet ke paass b chala jayega 

    const itemEvent = (event) => {
        setInputList(event.target.value);
    };

    const listOfItems = () => {
        setItems((oldItems) => {
            return [...oldItems, inputList];
        });
        setInputList("");
    } 

    const deleteItems = (id) => {
        setItems((oldItems) => {
            return oldItems.filter((arrElement, index) => {
                return index !== id; //yaha index jo h vo id se match nhi ho rha h to me un sabko in the form of array return krva rha hu 
            })
        })
    }
    return(
        <>
            <div className="main_div">
                <div className="center_div">
                    <br />
                    <h1>ToDO List</h1>
                    <br />
                    <input type="text" placeholder="Add a items" onChange={itemEvent} value={inputList} />
                    <button onClick={listOfItems}> + </button>

                    <ol>
                        {/* <li> {inputList} </li> */}
                        {
                            Items.map( (itemval, index) => {
                                return <ToDoLists key={index} id={index} text={itemval} onSelect={deleteItems}/>

                            })
                        }
                    </ol>
                </div>
            </div>
        </>
    );
}

export default App;